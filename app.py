from flask import Flask, request, render_template
import pickle

app = Flask(__name__)

model_file = open('model.pkl', 'rb')
model = pickle.load(model_file, encoding='bytes')

model_file_kh = open('model_kh.pkl', 'rb')
model_kh = pickle.load(model_file_kh, encoding='bytes')

model_file_jg = open('model_jg.pkl', 'rb')
model_jg = pickle.load(model_file_jg, encoding='bytes')

model_file_kt = open('model_kt.pkl', 'rb')
model_kt = pickle.load(model_file_kt, encoding='bytes')

model_file_kp = open('model_kp.pkl', 'rb')
model_kp = pickle.load(model_file_kp, encoding='bytes')

model_file_kr = open('model_kr.pkl', 'rb')
model_kr = pickle.load(model_file_kr, encoding='bytes')

@app.route('/', methods=['GET'])
def index():
    komoditas = ['Padi', 'Kacang Hijau', 'Jagung', 'Kacang Tanah','Ketela Pohon','Ketela Rambat']
    return render_template('index.html', hasil_panen=0, komoditas=komoditas,pangan='')

@app.route('/predict', methods=['POST'])
def predict():
    '''
    Predict the insurance cost based on user inputs
    and render the result to the html page
    '''
    #age, sex, smoker = [x for x in request.form.values()]

    #data = []

    #data.append(int(age))
    #if sex == 'Laki-laki':
    #    data.extend([0, 1])
    #else:
    #    data.extend([1, 0])

    #if smoker == 'Ya':
    #    data.extend([0, 1])
    #else:
    #    data.extend([1, 0])
    
    #prediction = model.predict([data])
    user_input = float(request.form['Luas'])
    pangan = request.form['komoditas']
    if pangan == 'Padi' :
        prediction = model.predict([1.0, user_input])
        output = round(prediction[0], 2)
    elif pangan == 'Kacang Hijau' :
        prediction = model_kh.predict([1.0, user_input])
        output = round(prediction[0], 2)
    elif pangan == 'Jagung' :
        prediction = model_jg.predict([1.0, user_input])
        output = round(prediction[0], 2)
    elif pangan == 'Kacang Tanah' :
        prediction = model_kt.predict([1.0, user_input])
        output = round(prediction[0], 2)
    elif pangan == 'Ketela Pohon' :
        prediction = model_kp.predict([1.0, user_input])
        output = round(prediction[0], 2)
    else:
        prediction = model_kr.predict([1.0, user_input])
        output = round(prediction[0], 2)

    komoditas = ['Padi', 'Kacang Hijau', 'Jagung', 'Kacang Tanah','Ketela Pohon','Ketela Rambat']
    #return render_template('index.html', insurance_cost=output, age=age, sex=sex, smoker=smoker)
    return render_template('index.html', hasil_panen=output,komoditas=komoditas,pangan=pangan)


if __name__ == '__main__':
    app.run(debug=True)